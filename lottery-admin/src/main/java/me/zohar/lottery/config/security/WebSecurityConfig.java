package me.zohar.lottery.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthorizationTokenFilter authenticationTokenFilter;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.cors().and()
		.csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
		.authorizeRequests()
		.antMatchers("/adminApi/login").permitAll()
		.antMatchers("/adminApi/masterControl/getInviteRegisterSetting").permitAll()
		.antMatchers("/adminApi/userAccount/register").permitAll()
		.antMatchers("/adminApi/userAccount/getUserAccountInfo").permitAll()
		.antMatchers("/adminApi/game/findAllOpenGame").permitAll()
		.antMatchers("/adminApi/game/findAllGameCategory").permitAll()
		.antMatchers("/adminApi/game/findAllGameSituation").permitAll()
		.antMatchers("/adminApi/game/findGameSituationByGameCategoryId").permitAll()
		.antMatchers("/adminApi/recharge/muspayCallback").permitAll()
		.antMatchers("/adminApi/betting/findTop50WinningRank").permitAll()
		.antMatchers("/adminApi/systemNotice/findTop5PublishedSystemNotice").permitAll()
		.antMatchers("/adminApi/lotteryInformation/**").permitAll()
		.antMatchers("/adminApi/issue/findLotteryHistory").permitAll()
		.antMatchers("/adminApi/game/findGameByGameCode").permitAll()
		.anyRequest().authenticated();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/adminApi/css/**", "/adminApi/images/**", "/adminApi/js/**", "/adminApi/plugins/**", "/adminApi/**.ico");
	}
	
}
